﻿/*
    The Amazing UwU Client 
 
    Documentation:
    Session: CKFKDPJFKOO ?
    User:
    Player: KHMGKFJJDCO ?
    Player Object:

    This is how like it usually is
*/

using System;
using System.Diagnostics;
using System.Threading;
using System.Globalization;

using VRLoader.Attributes;
using VRLoader.Modules;

using UnityEngine;
using VRC;
using VRC.Core;

using Framework.Configuration;

using Globals;

namespace Client
{
    [ModuleInfo("Divinity", "0.1", "Inari")]
    public class Main : VRModule
    {

        public void Start()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

            if (!ConfigMgr.Load("divinity.conf"))
            {
                Log.outError(LogFilter.Divinity, "Error Could Not Load Config");
                return;
            }
        }

        public void Awake()
        {

        }

        public void Update()
        {

        }
    }
}
