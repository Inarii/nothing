﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Constants
{
    public enum ScriptState
    {
        Disabled = 0,
        Enabled = 1
    }
}
