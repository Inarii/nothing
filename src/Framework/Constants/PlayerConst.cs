﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework.Constants
{
    public enum Visibility
    {
        Safety = 0,
        Show = 1,
        Hide = 2
    }
}
