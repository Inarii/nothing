﻿using System;
using System.Runtime.CompilerServices;

public class Cypher
{
    public static void Assert(bool value, string message = "", [CallerMemberName]string memberName = "")
    {
        if (!value)
        {
            if (!message.IsEmpty())
                Log.outFatal(LogFilter.Divinity, message);

            throw new Exception(memberName);
        }
    }
}