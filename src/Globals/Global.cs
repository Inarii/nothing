﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Scripting;

namespace Globals
{
    public static class Global
    {
        public static ScriptManager ScriptMgr { get { return ScriptManager.Instance; } }
    }
}
