﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using UnityEngine;

using Globals;

namespace Scripting
{
    [Script]
    public class FlyScript : PlayerScript
    {
        public FlyScript() : base("Fly") { }
        public void OnUpdate()
        {
            Transform cameraRotation = Camera.main.transform;
            if (Input.GetKey(KeyCode.W))
                transform.position += cameraRotation.transform.forward * MoveSpeed();
            if (Input.GetKey(KeyCode.S))
                transform.position -= cameraRotation.transform.forward * MoveSpeed();
            if (Input.GetKey(KeyCode.A))
                transform.position -= cameraRotation.transform.right * MoveSpeed();
            if (Input.GetKey(KeyCode.D))
                transform.position += cameraRotation.transform.right * MoveSpeed();
            if (Input.GetKey(KeyCode.Q))
                transform.position += cameraRotation.transform.up * MoveSpeed();
            if (Input.GetKey(KeyCode.E))
                transform.position -= cameraRotation.transform.up * MoveSpeed();
            if (Input.GetAxis("Vertical") != 0)
                transform.position += cameraRotation.transform.forward * .12f * Input.GetAxis("Vertical");
            if (Input.GetAxis("Horizontal") != 0)
                transform.position += cameraRotation.transform.right * MoveSpeed() * Input.GetAxis("Horizontal");
        }

        public static float MoveSpeed()
        {
            return Input.GetKey(KeyCode.LeftShift) ? .6f : .07f;
        }
    }
}
