﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using Framework.Configuration;
using Framework.Constants;
using Globals;

using UnityEngine;

namespace Scripting
{
    public class ScriptObject : MonoBehaviour
    {
        public ScriptObject(string name)
        {
            _name = name;
            enabled = bool.Parse(ConfigMgr.GetDefaultValue(name, "0"));
        }

        public string GetName() { return _name; }
        public void SetState(bool state) { enabled = state; }

        string _name;
    }

    public class PlayerScript : ScriptObject
    {
        public PlayerScript(string name) : base(name)
        {
            Global.ScriptMgr.AddScript(this);
        }

        public virtual void OnJoin() { }
        public virtual void OnLeave() { }
    }

    public class InstanceScript : ScriptObject
    {
        public InstanceScript(string name) : base(name)
        {
            Global.ScriptMgr.AddScript(this);
        }

        public virtual void OnPlayerEnterWorld() { }
        public virtual void OnPlayerLeaveWorld() { }

    }

    public class ScriptManager : Singleton<ScriptManager>
    {
        ScriptManager() { }

        public void Initialize()
        {
            uint oldMSTime = Time.GetMSTime();

            Log.outInfo(LogFilter.Divinity, "Loading Scripts");

            // Load all Script dlls
            LoadScripts();

            Log.outInfo(LogFilter.Divinity, $"Loaded {GetScriptCount()} C# scripts in {Time.GetMSTimeDiffToNow(oldMSTime)} ms");
        }

        public void LoadScripts()
        {
            var result = Directory.GetFiles(AppContext.BaseDirectory + "*.dll");
            if (result.Empty())
            {
                Log.outError(LogFilter.Divinity, "Could Not Find Any Script DLLs!");
                return;
            }

            foreach (var script in result)
            {
                Assembly assembly = Assembly.LoadFile(AppContext.BaseDirectory + script);
                if (assembly == null)
                {
                    Log.outError(LogFilter.Divinity, $"Error Loading Script: {script}");
                    continue;
                }

                foreach (var type in assembly.GetTypes())
                {
                    var attributes = (ScriptAttribute[])type.GetCustomAttributes<ScriptAttribute>();
                    if (attributes.Empty())
                    {
                        var baseType = type.BaseType;
                        while (baseType != null)
                        {
                            if (baseType == typeof(ScriptObject))
                            {
                                Log.outWarn(LogFilter.Divinity, "Script {0} does not have ScriptAttribute", type.Name);
                                continue;
                            }

                            baseType = baseType.BaseType;
                        }
                    }
                    else
                    {
                        var constructors = type.GetConstructors();
                        if (constructors.Length == 0)
                        {
                            Log.outError(LogFilter.Scripts, "Script: {0} contains no Public Constructors. Can't load script.", type.Name);
                            continue;
                        }

                        foreach (var attribute in attributes)
                        {
                            var genericType = type;
                            string name = type.Name;

                            bool validArgs = true;
                            int i = 0;
                            foreach (var constructor in constructors)
                            {
                                var parameters = constructor.GetParameters();
                                if (parameters.Length != attribute.Args.Length)
                                    continue;

                                foreach (var arg in constructor.GetParameters())
                                {
                                    if (arg.ParameterType != attribute.Args[i++].GetType())
                                    {
                                        validArgs = false;
                                        break;
                                    }
                                }

                                if (validArgs)
                                    break;
                            }

                            if (!validArgs)
                            {
                                Log.outError(LogFilter.Scripts, "Script: {0} contains no Public Constructors with the right parameter types. Can't load script.", type.Name);
                                continue;
                            }

                            if (!attribute.Name.IsEmpty())
                                name = attribute.Name;

                            if (attribute.Args.Empty())
                                Activator.CreateInstance(genericType);
                            else
                                Activator.CreateInstance(genericType, new object[] { name }.Combine(attribute.Args));

                            if (!attribute.Name.IsEmpty())
                                name = attribute.Name;

                            Activator.CreateInstance(genericType, name, attribute.Args);
                        }
                    }
                }
            }
        }

        public void UnloadScripts()
        {
            foreach (DictionaryEntry entry in ScriptStorage)
            {
                IScriptRegistry scriptRegistry = (IScriptRegistry)entry.Value;
                scriptRegistry.Unload();
            }

            ScriptStorage.Clear();
        }

        public void IncrementScriptCount() { ++_ScriptCount; }
        public uint GetScriptCount() { return _ScriptCount; }

        public void ForEach<T>(Action<T> a) where T : ScriptObject
        {
            var reg = GetScriptRegistry<T>();
            if (reg == null || reg.Empty())
                return;

            foreach (var script in reg.GetStorage())
                a.Invoke(script);
        }
        public bool RunScriptRet<T>(Func<T, bool> func, uint id, bool ret = false) where T : ScriptObject
        {
            return RunScriptRet<T, bool>(func, id, ret);
        }
        public U RunScriptRet<T, U>(Func<T, U> func, uint id, U ret = default) where T : ScriptObject
        {
            var reg = GetScriptRegistry<T>();
            if (reg == null || reg.Empty())
                return ret;

            var script = reg.GetScriptById(id);
            if (script == null)
                return ret;

            return func.Invoke(script);
        }

        public void RunScript<T>(Action<T> a, uint id) where T : ScriptObject
        {
            var reg = GetScriptRegistry<T>();
            if (reg == null || reg.Empty())
                return;

            var script = reg.GetScriptById(id);
            if (script != null)
                a.Invoke(script);
        }
        public void AddScript<T>(T script) where T : ScriptObject
        {
            Cypher.Assert(script != null);

            if (!ScriptStorage.ContainsKey(typeof(T)))
                ScriptStorage[typeof(T)] = new ScriptRegistry<T>();

            GetScriptRegistry<T>().AddScript(script);
        }

        ScriptRegistry<T> GetScriptRegistry<T>() where T : ScriptObject
        {
            if (ScriptStorage.ContainsKey(typeof(T)))
                return (ScriptRegistry<T>)ScriptStorage[typeof(T)];

            return null;
        }

        uint _ScriptCount;
        Hashtable ScriptStorage = new Hashtable();

        public interface IScriptRegistry
        {
            void Unload();
        }

        public class ScriptRegistry<TValue> : IScriptRegistry where TValue : ScriptObject
        {
            public void AddScript(TValue script)
            {
                Cypher.Assert(script != null);

                ScriptMap[_scriptIdCounter++] = script;
                Global.ScriptMgr.IncrementScriptCount();
            }

            public TValue GetScriptById(uint id)
            {
                return ScriptMap.LookupByKey(id);
            }

            public bool Empty()
            {
                return ScriptMap.Empty();
            }

            public List<TValue> GetStorage()
            {
                return ScriptMap.Values.ToList();
            }

            public void Unload()
            {
                ScriptMap.Clear();
            }

            uint _scriptIdCounter;
            Dictionary<uint, TValue> ScriptMap = new Dictionary<uint, TValue>();
        }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ScriptAttribute : Attribute
    {
        public ScriptAttribute(string name = "", params object[] args)
        {
            Name = name;
            Args = args;
        }

        public string Name { get; private set; }
        public object[] Args { get; private set; }
    }
}
